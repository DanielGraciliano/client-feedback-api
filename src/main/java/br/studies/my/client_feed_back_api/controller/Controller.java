package br.studies.my.client_feed_back_api.controller;

import org.springframework.web.bind.annotation.RestController;

import br.studies.my.client_feed_back_api.model.Feedback;
import br.studies.my.client_feed_back_api.repository.ClientFeedbackRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;



@RestController
public class Controller {

    @Autowired
    private ClientFeedbackRepository repository;

    @GetMapping("/evaluations")
    public List<Feedback> allEvaluates(){
        return repository.findAll();
    }


    @PostMapping("/evaluation")
	public Feedback evaluate(@RequestBody Feedback feedback){
        repository.save(feedback);
		return feedback;
	}

}
