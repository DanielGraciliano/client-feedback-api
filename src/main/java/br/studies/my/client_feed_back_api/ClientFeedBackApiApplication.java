package br.studies.my.client_feed_back_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientFeedBackApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientFeedBackApiApplication.class, args);
	}

}
