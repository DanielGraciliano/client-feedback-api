package br.studies.my.client_feed_back_api.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection =  "feedbacks")
public class Feedback {
    
    @Id
    private String id;

    private String clientName;
    private String serviceName;
    private short rating;

    public Feedback(String id, String clientName, String serviceName, short rating){
        super();

        this.id = id;
        this.clientName = clientName;
        this.serviceName = serviceName;
        this.rating = rating;

    }

    public String getId(){
        return id;
    }

    public String setId(String id){
        return id;
    }

    public String getClientName(){
        return clientName;
    }

    public void setClientName(String clientName){
        this.clientName = clientName;
    }

    public String getServiceName() {
        return serviceName;
    }
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public short getRating(){
        return rating; 
    }

    public void setRating(short rating) {
        this.rating = rating;
    }
    
}
