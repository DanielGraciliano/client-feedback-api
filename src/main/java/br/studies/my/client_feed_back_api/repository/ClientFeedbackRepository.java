package br.studies.my.client_feed_back_api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.studies.my.client_feed_back_api.model.Feedback;
import java.util.List;


public interface ClientFeedbackRepository extends MongoRepository<Feedback, String> {

    List<Feedback> findByRating(short rating);
    
}
